Source: php-doc
Section: doc
Priority: optional
Maintainer: Debian PHP Maintainers <team+pkg-php@tracker.debian.org>
Uploaders: Athos Ribeiro <athos.ribeiro@canonical.com>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: php-cli, php-sqlite3, php-xml, jing
Standards-Version: 4.6.2
Homepage: http://docs.php.net/manual/en/
Vcs-Browser: https://salsa.debian.org/php-team/php-doc
Vcs-Git: https://salsa.debian.org/php-team/php-doc.git
Rules-Requires-Root: no

Package: php-doc
Architecture: all
Multi-Arch: foreign
Recommends: php-cli
Depends: ${misc:Depends}
Description: Documentation for PHP
 This package provides the documentation for the PHP scripting language.
 .
 PHP, which stands for "PHP: Hypertext Preprocessor" is a widely-used Open
 Source general-purpose scripting language that is especially suited for Web
 development and can be embedded into HTML. Its syntax draws upon C, Java, and
 Perl, and is easy to learn. The main goal of the language is to allow web
 developers to write dynamically generated web pages quickly, but you can do
 much more with PHP.
 .
 This manual consists primarily of a function reference, but also contains a
 language reference, explanations of some of PHP's major features, and other
 supplemental information.
