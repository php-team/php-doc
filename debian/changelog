php-doc (20241205~git.dfcbb86+dfsg-1) unstable; urgency=medium

  * d/copyright: remove pre-built jing java jar
  * New upstream version 20241205~git.dfcbb86+dfsg
  * d/control: add jing as a build dependency
  * d/p/system-jing.patch: use system jing
  * d/patches: refresh patches

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Fri, 06 Dec 2024 11:07:48 -0300

php-doc (20240421~git.dacdc95+dfsg-1) unstable; urgency=medium

  * New upstream version 20240421~git.dacdc95+dfsg
  * d/patches: refresh patches

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Thu, 25 Apr 2024 20:31:55 -0300

php-doc (20240317~git.0aaa1ae+dfsg-1) unstable; urgency=medium

  * New upstream version 20240317~git.0aaa1ae+dfsg
  * d/patches: refresh patches

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Thu, 21 Mar 2024 20:48:12 -0300

php-doc (20231206~git.66ff996+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Joao Nobrega ]
  * d/p/reproducible-build.patch: apply patch to fix reproducible build.
    Thanks to Chris Lamb. (Closes: #1056398)

  [ Athos Ribeiro ]
  * d/copyright: Update copyright date

 -- Joao Nobrega <joaopedrobsb3@gmail.com>  Sat, 06 Jan 2024 18:21:03 -0300

php-doc (20231206~git.66ff996+dfsg-1) unstable; urgency=medium

  * New upstream version 20231206~git.66ff996+dfsg
  * d/copyright:
    - Remove file deleted upstream
    - Update copyright dates

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Fri, 15 Dec 2023 16:00:12 -0300

php-doc (20231115~git.99fbf5d+dfsg-1) unstable; urgency=medium

  * d/control: set Multi-Arch to foreign
  * New upstream version 20231115~git.99fbf5d+dfsg

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Tue, 21 Nov 2023 04:26:20 -0300

php-doc (20231111~git.f333b4a+dfsg-1) unstable; urgency=medium

  * New upstream version 20231111~git.f333b4a+dfsg
  * Drop patch for broken links

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Mon, 13 Nov 2023 09:06:41 -0300

php-doc (20231017~git.cce6f7f+dfsg-2) unstable; urgency=medium

  * d/p/fix-broken-phar-constant-links.patch: fix broken Phar constant links.

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Wed, 08 Nov 2023 07:00:24 -0300

php-doc (20231017~git.cce6f7f+dfsg-1) unstable; urgency=medium

  * New upstream release. (Closes: #903999, #821695)
  * d/control:
    - Use debhelper-compat 13.
    - Update Standards-Version to 4.6.2.
    - Update dependencies to reflect recent php stack.
    - Update maintainer information.
    - Add missing php-xml dependency.
    - Add VCS information.
    - Add Rules-Requires-root field.
  * d/compat: remove file.
  * d/copyright:
    - Remove non-free files.
    - Update copyright data.
    - Update source information.
    - Update Format URL to use HTTPS.
  * d/rules:
    - Rely on modern debhelper hooks.
    - Render documentation with the PHP package.
      (Closes: #737713, #766882, #734145)
  * d/clean: remove build leftovers.
  * d/watch: track multiple upstream tarballs.
  * d/repack-doc-base.sh: add script to repack doc-base with the expected
    directory structure.
  * d/README.source: document Multiple Upstream Tarballs structure.
  * d/source/lintian-overrides: ignore license-problem-php-license.
  * d/php-doc.docs: install documentation through dh_installdocs.
  * d/tests: test docs for broken local URLs.
  * d/debian.css: use modified maint-guide stylesheet.
  * d/patches:
    - Remove xkcd mediaobjects.
    - Replace non-free stylesheets.
  * d/gbp.conf:
    - Default to pristine-tar = False. gbp generates dangling tree references
      otherwise. See #1031693 for further reference.
    - Set debian-branch to debian/main.

 -- Athos Ribeiro <athos.ribeiro@canonical.com>  Tue, 31 Oct 2023 09:18:44 -0300

php-doc (20140201-1) unstable; urgency=medium

  * New upstream release
  * Upload under the Debian PHP Maintainers team umbrella

 -- Lior Kaplan <kaplan@debian.org>  Tue, 11 Feb 2014 02:02:39 +0200

php-doc (20131001-1) unstable; urgency=low

  * New upstream release
  * Upgrade PhD to 1.1.16
    - Should reduce memory consumption (Closes: #718086)
    - Requires PHP sqlite extension, dependency added.
  * Switch to dpkg-source 3.0 (quilt) format

 -- Lior Kaplan <kaplan@debian.org>  Tue, 01 Oct 2013 20:39:44 +0200

php-doc (20100521-2) unstable; urgency=low

  * Fix the clean part on debian/rules. (Closes: #583594)

 -- Lior Kaplan <kaplan@debian.org>  Sat, 29 May 2010 21:56:21 +0300

php-doc (20100521-1) unstable; urgency=low

  * New upstream release (Closes: #573505)
    - Removes build dependency on sp.
    - Doesn't provide a php.ini file (Closes: #533998)
  * Fix bashism in debian/rules (Closes: #535408)
  * Upgrade PhD to version 0.4.8 (version 1.0.0 is a PEAR package).
  * Bump standards-version to 3.8.4 (no changes needed).
  * Override lintian copyright-refers-to-problematic-php-license tag
    as code from the PHP group is provided.

 -- Lior Kaplan <kaplan@debian.org>  Mon, 24 May 2010 17:21:24 +0300

php-doc (20081024-1) unstable; urgency=low

  * New upstream release (Closes: #467434).
    - License changed to CC-BY v3.0.
  * Debian/copyright:
    - Update Upstream Authors list.
    - Change license from OPL to CC-BY v3.0.
    - Add license info on all of the old build scripts.
    - Add PhD's license.
  * debian/control:
    - Return package to main, as the license was changed.
    - Bump standards-version to 3.8.0 (no changes needed).
    - Remove PHP4 from the short description.
    - Change Build-Depends-Indep to only php5-cli and sp as the new build
      system have less requirements.
  * debian/rules:
    - Change build system from autoconf/make to PhD.

 -- Lior Kaplan <kaplan@debian.org>  Sat, 25 Oct 2008 20:05:18 +0200

php-doc (20070501-1) unstable; urgency=low

  * New upstream release
    - Fixes /usr/share/doc/php-doc/html/indexes.html (Closes: #404908).
    - Fixes /usr/share/doc/php-doc/html/faq.general.html (Closes: #411725).
  * debian/control
    - Remove transitional package phpdoc.
    - Recommend php5-cli.

 -- Lior Kaplan <kaplan@debian.org>  Tue, 01 May 2007 23:00:00 +0300

php-doc (20061001-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Move debhelper to build depends, and use version 5.
    - Update Standards-Version to 3.7.2 (no changes needed)

 -- Lior Kaplan <kaplan@debian.org>  Sun,  1 Oct 2006 13:04:45 +0200

php-doc (20060408-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Rename package to php-doc (Closes: #314202)
    - Keep phpdoc as a transition package for Sarge->Etch upgrade
    - Update Build-Depends-Indep to php5 and debhelper4
    - Update Standards-Version to 3.6.2 (no changes needed)
    - Update my maintainer address to @debian.org
  * Remove upstream's README from the package (Closes: #310242)

 -- Lior Kaplan <kaplan@debian.org>  Sat,  8 Apr 2006 20:04:39 +0300

phpdoc (20050512-1) unstable; urgency=low

  * New maintainer. Closes: #288741, #306670
  * New upstream release. Closes: #253982, #288422
  * Upstream fixed URL to GNU date syntax. Closes: #250693
  * Acknowledge NMUs by Sebastian Muszynski & myself. Closes: #230677, #282274
  * debian/copyright: update license from GPL to Open Publication License.
  * debian/control:
    - move to non-free due to license change.
    - Build-Depends-Indep: add autoconf, xsltproc & libxml2-utils.
    - Add my sponsor, Shaul Karl <shaul@debian.org> to Uploaders.
  * debian/rules: add clean commands (thanks for Jeroen van Wolffelaar)

 -- Lior Kaplan <webmaster@guides.co.il>  Thu, 12 May 2005 18:31:54 +0300

phpdoc (20030911-1.2) unstable; urgency=high

  * Non-maintainer upload.
  * debian/control:
    - remove openjade from Build-Depends-Indep. (Closes: #282274)
    - fix standards version to 3.6.1
  * debian/README.Debian: fix spelling errors
  * debian/copyright: remove extra braces (fixes lintian error)

 -- Lior Kaplan <webmaster@guides.co.il>  Fri,  6 May 2005 19:44:18 +0300

phpdoc (20030911-1.1) unstable; urgency=low

  * NMU during BSP.
  * Build-Depends fixed. (closes: #230677)

 -- Sebastian Muszynski <do2ksm@linkt.de>  Sun, 21 Mar 2004 03:14:48 +0100

phpdoc (20030911-1) unstable; urgency=low

  * New upstream release.

 -- Petr Cech <cech@debian.org>  Thu, 11 Sep 2003 08:33:02 +0200

phpdoc (20030716-1) unstable; urgency=low

  * New upstream version.

 -- Petr Cech <cech@debian.org>  Wed, 16 Jul 2003 14:46:15 +0200

phpdoc (20030623-1) unstable; urgency=low

  * New upstream release.

 -- Petr Cech <cech@debian.org>  Mon, 23 Jun 2003 23:17:31 +0200

phpdoc (20030518-1) unstable; urgency=low

  * New upstream release.

 -- Petr Cech <cech@debian.org>  Sun, 18 May 2003 16:31:28 +0200

phpdoc (20030303-1) unstable; urgency=low

  * New upstream release.

 -- Petr Cech <cech@debian.org>  Mon,  3 Mar 2003 00:20:49 +0100

phpdoc (20030209-1) unstable; urgency=low

  * New upstream relese.
  * debian/control: s/docbook-stylesheets/docbook-dsssl/

 -- Petr Cech <cech@debian.org>  Sun,  9 Feb 2003 19:31:33 +0100

phpdoc (20030104-1) unstable; urgency=low

  * New upstream relese. (try to upload it this time :-D)

 -- Petr Cech <cech@debian.org>  Wed,  8 Jan 2003 21:45:36 +0100

phpdoc (20021116-1) unstable; urgency=low

  * New CVS snapshot.

 -- Petr Cech <cech@debian.org>  Sat, 16 Nov 2002 18:09:14 +0100

phpdoc (20020927-1) unstable; urgency=low

  * New CVS snapshot

 -- Petr Cech <cech@debian.org>  Fri, 27 Sep 2002 14:02:28 +0200

phpdoc (20020820-1) unstable; urgency=low

  * New CVS snapshot
  * Build-depends-indep: php4-cgi (closes: #155358)

 -- Petr Cech <cech@debian.org>  Tue, 20 Aug 2002 21:45:01 +0200

phpdoc (20020727-1) unstable; urgency=low

  * New CVS snapshot

 -- Petr Cech <cech@debian.org>  Sat, 27 Jul 2002 15:02:08 +0200

phpdoc (20020618-1) unstable; urgency=low

  * New CVS snapshot

 -- Petr Cech <cech@debian.org>  Wed, 19 Jun 2002 18:05:25 +0200

phpdoc (20020515-1) unstable; urgency=low

  * New CVS snapshot

 -- Petr Cech <cech@debian.org>  Sat, 18 May 2002 00:44:05 +0200

phpdoc (20020310-1) unstable; urgency=low

  * New CVS snapshot.

 -- Petr Cech <cech@debian.org>  Sun, 10 Mar 2002 19:12:55 +0100

phpdoc (20020222-1) unstable; urgency=low

  * New CVS snapshot.

 -- Petr Cech <cech@debian.org>  Fri, 22 Feb 2002 23:29:58 +0100

phpdoc (20020106-1) unstable; urgency=low

  * New CVS snapshot.

 -- Petr Cech <cech@debian.org>  Sun,  6 Jan 2002 15:20:16 +0100

phpdoc (20011213-1) unstable; urgency=low

  * New CVS snapshot

 -- Petr Cech <cech@debian.org>  Thu, 13 Dec 2001 23:30:22 +0100

phpdoc (20011025-1) unstable; urgency=low

  * New CVS snapshot.
  * Build-Depends: openjade | jade, docbook-xsl-stylesheets
  * Add path to docbook.xsl
  * Makefile.in : remove some autogenerated files

 -- Petr Cech <cech@debian.org>  Thu, 25 Oct 2001 20:46:07 +0200

phpdoc (20010818-1) unstable; urgency=low

  * New CVS snapshot.

 -- Petr Cech <cech@debian.org>  Sat, 18 Aug 2001 11:31:20 +0200

phpdoc (20010626-1) unstable; urgency=low

  * New CVS snapshot.
  * debian/prerm: remove spurious .dhelp file. grr! (closes: #101248)

 -- Petr Cech <cech@debian.org>  Thu, 28 Jun 2001 10:21:58 +0200

phpdoc (20010423-1) unstable; urgency=low

  * New CVS snapshot.

 -- Petr Cech <cech@debian.org>  Mon, 23 Apr 2001 10:15:14 +0200

phpdoc (20010325-1) unstable; urgency=low

  * Doh. Now there is no manual.html, but index.html (closes: #90820)

 -- Petr Cech <cech@debian.org>  Sun, 25 Mar 2001 21:54:26 +0200

phpdoc (20010322-1) unstable; urgency=low

  * New CVS snapshot.

 -- Petr Cech <cech@debian.org>  Thu, 22 Mar 2001 08:56:03 +0100

phpdoc (20010222-1) unstable; urgency=low

  * New CVS snapshot.
  * Standards-Version: 3.5.2 (no change)

 -- Petr Cech <cech@debian.org>  Thu, 22 Feb 2001 00:59:02 +0100

phpdoc (20010115-1) unstable; urgency=low

  * New upstream version
  * debian/control: Section: Apps/Programming (closes: #82232)
  * debian/rules: link manual.html to index.html (closes: #82231)

 -- Petr Cech <cech@debian.org>  Mon, 15 Jan 2001 17:11:37 +0100

phpdoc (20001218-1) unstable; urgency=low

  * New upstream version

 -- Petr Cech <cech@debian.org>  Mon, 18 Dec 2000 02:56:06 +0100

phpdoc (20001124-1) unstable; urgency=low

  * Initial Release.

 -- Petr Cech <cech@debian.org>  Mon, 27 Nov 2000 09:18:57 +0100

Local variables:
mode: debian-changelog
add-log-mailing-address "cech@debian.org"
End:
